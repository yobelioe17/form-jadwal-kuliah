from django import forms
from .models import PostModel

class PostForm(forms.ModelForm):
    class Meta:
        model = PostModel
        fields = {
            'nama',
            'hari',
            'tanggal',
            'jam',
            'tempat',
            'kategori',
        }

        widgets = {
            'nama' : forms.TextInput(
                attrs={
					'class':'form-control',
					'placeholder':'Nama Kegiatan'
                }
            ),
			
			'hari' : forms.TextInput(
                attrs={
					'class':'form-control',
                    'placeholder':'Eg: Senin'
                }
            ),
			'tanggal' : forms.TextInput(
                attrs={
					'class':'form-control',
                    'type' : 'date',
                }
            ),
            'jam' : forms.TextInput(
                attrs={
					'class':'form-control',
                    'type' : 'time'
                }
            ),
			
			'tempat' : forms.TextInput(
                attrs={
					'class':'form-control',
                    'placeholder':'Eg: Pulau Kapuk'
                }
            ),
			
			'kategori' : forms.TextInput(
                attrs={
					'class':'form-control',
                    'placeholder':'Eg: Santuy'
                }
            ),
        }
