from django.db import models

# Create your models here.
class PostModel(models.Model):
    nama = models.CharField(max_length = 50)
    hari = models.CharField(max_length = 10)
    tanggal = models.DateField()
    jam = models.TimeField()   
    tempat = models.CharField(max_length = 100)  
    kategori = models.CharField(max_length = 30)

