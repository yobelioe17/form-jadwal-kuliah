from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import PostForm
from .models import PostModel


def delete(request, delete_id):
    PostModel.objects.filter(id = delete_id).delete()
    return redirect('list')


def list(request):
    posts = PostModel.objects.all()

    context = {
        'page_title':'Semua Post',
        'posts':posts,
    }

    return render(request, 'list.html', context)

def create(request):
    post_form = PostForm(request.POST or None)
    if request.method == "POST":
        if post_form.is_valid():
            post_form.save()

            return redirect('list')

    context = {
        'page_title':'Create Post',
        'post_form': post_form,
    }

    return render(request, 'create.html', context)
    
# Create your views here.
def index(request):
    return render(request, 'Home.html')

# Create your views here.
def index_2(request):
    return render(request, 'Intro.html')

# Create your views here.
def index_3(request):
    return render(request, 'Skills.html')

# Create your views here.
def index_4(request):
    return render(request, 'Activity.html')

# Create your views here.
def index_5(request):
    return render(request, 'Achieve.html')

# Create your views here.
def index_6(request):
    return render(request, 'Contact.html')

