from django.urls import include, path
from . import views

urlpatterns = [
    path('delete/<int:delete_id>', views.delete, name='delete'), 
    path('', views.list, name='list'),
    path('create/', views.create, name='create'),
]
